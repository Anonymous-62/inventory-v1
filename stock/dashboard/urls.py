"""stock URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from dashboard import views
urlpatterns = [
    path('',views.dashboard,name='dashboard'),
    path('product/',views.createProduct,name='product'),
    path('product/create/',views.createProduct,name='create'),
    path('product/update/',views.updateProduct,name='update'),
    path('product/return/',views.returned_product,name='return'),
    path('product/return/update',views.update_retured_product,name='return__update'),
    path('customers/',views.createCustomer,name='customers'),
    path('customer/update/',views.cutomer_update,name='customer_update'),
    path('sumOfQuantity/',views.get_sum_of_quantity,name='sumOfQuantity'),
    path('sale/',views.sale,name='sale'),
    path('sale/update/',views.sale_update,name='update_sale'),
    path('sale/return/',views.sales_Return,name='salesReturn'),
    path('sales/return/update/',views.updateSalesReturn,name='update_sale_return')
]
